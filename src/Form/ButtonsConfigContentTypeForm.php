<?php

namespace Drupal\buttons_config\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure form_id settings.
 */
class ButtonsConfigContentTypeForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'buttons_config.node.settings';

  /**
   * Summary.
   *
   * Get Form Id.
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'buttons_config_node_settings';
  }

  /**
   * Summary.
   *
   * Get Config settings.
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Form table name.
   *
   * @var string
   */
  const FORM_TABLE = 'form_ids';

  /**
   * Column form_id.
   *
   * @var string
   */
  const FORM_COLUMN_FORM_ID = 'form_id';

  /**
   * Column form_id.
   *
   * @var string
   */
  const FORM_COLUMN_FORM_TYPE = 'form_type';

  /**
   * Column enabled.
   *
   * @var string
   */
  const FORM_COLUMN_ENABLED = 'enabled';

  /**
   * Column role.
   *
   * @var string
   */
  const FORM_COLUMN_CUSTOM_TEXT = 'custom_text';

  /**
   * Summary.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   *   The entity type manager.
   */
  protected $entityTypeManager;

  /**
   * Summary.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Summary.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The conteiner interface service.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Summary.
   *
   * Build Form function.
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $form[self::FORM_TABLE] = [
      '#type' => 'table',
      '#header' => [
        self::FORM_COLUMN_FORM_ID => $this->t('Content Type'),
        self::FORM_COLUMN_FORM_TYPE => $this->t('Form'),
        self::FORM_COLUMN_ENABLED => $this->t('Enabled'),
        self::FORM_COLUMN_CUSTOM_TEXT => $this->t('Custom Text'),
      ],
    ];

    $form_ids = $config->get('form_ids');
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    $entities = [];
    foreach ($content_types as $key => $values) {
      $entities["node_" . $key] = $values->label();
    }

    $form_types = ["Edit", "Save"];

    if (!is_null($form_ids)) {
      foreach ($form_ids as $custom_text => $options) {
        $form[self::FORM_TABLE][$custom_text][self::FORM_COLUMN_FORM_ID] = [
          '#type' => 'select',
          '#multiple' => FALSE,
          '#options' => $entities,
          '#default_value' => $form_ids[$custom_text][self::FORM_COLUMN_FORM_ID] ?: '',
        ];
        $form[self::FORM_TABLE][$custom_text][self::FORM_COLUMN_FORM_TYPE] = [
          '#type' => 'select',
          '#multiple' => FALSE,
          '#options' => $form_types,
          '#default_value' => $form_ids[$custom_text][self::FORM_COLUMN_FORM_TYPE] ?: '',
        ];
        $form[self::FORM_TABLE][$custom_text][self::FORM_COLUMN_ENABLED] = [
          '#type' => 'checkbox',
          '#default_value' => $form_ids[$custom_text][self::FORM_COLUMN_ENABLED] ?: FALSE,
        ];
        $form[self::FORM_TABLE][$custom_text][self::FORM_COLUMN_CUSTOM_TEXT] = [
          '#type' => 'textfield',
          '#maxlength' => 50,
          '#default_value' => $form_ids[$custom_text][self::FORM_COLUMN_CUSTOM_TEXT] ?: 'Custom text',
        ];
      }
    }
    $form[self::FORM_TABLE][0][self::FORM_COLUMN_FORM_ID] = [
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' => $entities,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => '',
    ];
    $form[self::FORM_TABLE][0][self::FORM_COLUMN_FORM_TYPE] = [
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' => $form_types,
      '#default_value' => '',
    ];
    $form[self::FORM_TABLE][0][self::FORM_COLUMN_ENABLED] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
    ];
    $form[self::FORM_TABLE][0][self::FORM_COLUMN_CUSTOM_TEXT] = [
      '#type' => 'textfield',
      '#maxlength' => 50,
      '#default_value' => '',
      '#placeholder' => 'Custom text',
      '#description' => $this->t('Text that will appear on the Save button.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Summary.
   *
   * Submit Form function.
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $table = $form_state->getValue(static::FORM_TABLE);
    $aux_table = [];

    foreach ($table as $options) {
      if ($options[self::FORM_COLUMN_CUSTOM_TEXT]) {
        if ($options[self::FORM_COLUMN_FORM_TYPE] == 0) {
          $button_type = '_edit';
        }
        else {
          $button_type = '_save';
        }

        $aux_table[$options[self::FORM_COLUMN_FORM_ID] . $button_type][self::FORM_COLUMN_FORM_TYPE] = $options[self::FORM_COLUMN_FORM_TYPE];
        $aux_table[$options[self::FORM_COLUMN_FORM_ID] . $button_type][self::FORM_COLUMN_FORM_ID] = $options[self::FORM_COLUMN_FORM_ID];
        $aux_table[$options[self::FORM_COLUMN_FORM_ID] . $button_type][self::FORM_COLUMN_ENABLED] = $options[self::FORM_COLUMN_ENABLED];
        $aux_table[$options[self::FORM_COLUMN_FORM_ID] . $button_type][self::FORM_COLUMN_CUSTOM_TEXT] = $options[self::FORM_COLUMN_CUSTOM_TEXT];
      }
    }

    $this->configFactory->getEditable(static::SETTINGS)
      ->set(static::FORM_TABLE, $aux_table)
      ->save();
  }

}
