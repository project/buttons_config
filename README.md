# Buttons Configuration

## Description

The Buttons configuration module provides an easy way to configure the text
of node forms, both save and edit. The module has a configuration page where
site administrator can choose which content and/or media types will have their
"Save" button text changed, in the add or edit forms.

## Requirements

This module requires no modules outside of Drupal core.
However, the Media and Comment modules must be enabled.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. First navigate to `admin/config/content/buttons-config`;
2. Select the content type or media type;
3. Select if you want the change to be reflect on the Add or Edit form (both);
4. Input the text you want to be displayed in the save button;
5. Click Save Configuration.
6. Navigate to the content/media type form and the save button will now display
   the text inputted before.


## Maintainers

- Nelson Alves - [nsalves](https://www.drupal.org/u/nsalves)
- Nuno Ramos - [-nrzr-](https://www.drupal.org/u/nrzr)
- André Diogo - [andrerdiogo](https://www.drupal.org/u/andrerdiogo)
- Débora Antunes - [dgaspara](https://www.drupal.org/u/dgaspara)
- João Marques - [joaomarques736](https://www.drupal.org/u/joaomarques736)
- Ricardo Tenreiro de Sá - [ricardotenreiro](https://www.drupal.org/u/ricardotenreiro)
- Tiago Pastor - [tiagopastor](https://www.drupal.org/u/tiagopastor)
