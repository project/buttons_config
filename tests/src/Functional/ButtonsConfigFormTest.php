<?php

namespace Drupal\Tests\buttons_config\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group multiple_select
 */
class ButtonsConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'buttons_config',
    'node',
    'media',
    'comment',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'seven';

  /**
   * Test access to configurations page.
   */
  public function testAccessConfigPage() {
    $account = $this->drupalCreateUser([
      'admin buttons config',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/config/content/buttons-config');
    $this->assertSession()->pageTextContains('Content Types');
    $this->assertSession()->pageTextContains('Media Types');
    $this->assertSession()->pageTextContains('Comment Types');
    $this->drupalGet('/admin/config/content/buttons-config/content-type');
    $this->assertSession()->pageTextContains('Change Form Button Text in Content Types');
    $this->assertSession()->buttonExists('Save configuration');
    $this->drupalGet('/admin/config/content/buttons-config/media-type');
    $this->assertSession()->pageTextContains('Change Form Button Text in Media Types');
    $this->assertSession()->buttonExists('Save configuration');
    $this->drupalGet('/admin/config/content/buttons-config/comment-type');
    $this->assertSession()->pageTextContains('Change Form Button Text in Comment Types');
    $this->assertSession()->buttonExists('Save configuration');
  }

}
